//  初始化 state
const state = {

    /**
     * Oauth2授权地址
     */
    oauthAuthorizeUrl : process.env.VUE_APP_OAUTH_AUTHORIZATION_URL,

};

const getters = {

    getOauthAuthorizeUrl : state => state.oauthAuthorizeUrl

};

const mutations = {

};

const actions = {

};

export default {
    state : state,
    getters : getters,
    mutations : mutations,
    actions : actions
};
