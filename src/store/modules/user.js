import $store from '../index';
import { getMyInfo } from '../../apis/index';
//  初始化 state
const state = sessionStorage.getItem('user_state') ? JSON.parse(sessionStorage.getItem('user_state')) : {
    myInfo : {}
};

const getters = {
    //  获取当前开发者信息
    getMyInfo : state => state.myInfo,
    isLogin : state => {
        for (let key in state.myInfo){
            if (Object.prototype.hasOwnProperty.call(state.myInfo, key)){
                return true;
            }
        }
        if (!$store.getters.getToken){
            return false;
        }
        //  获取用户信息
        getMyInfo().then(res => $store.commit('setUserInfo',res.data));
        return true;
    },
    isNewUser : state => {
        return !state.myInfo['auditFlag'] || state.myInfo['auditFlag'] === 'sleep';
    },
};

const mutations = {
    setUserInfo : (state,params) => state.myInfo = Object.assign(state.myInfo,params),
    delUserInfo : (state) => state.myInfo = {}
};

const actions = {

};

export default {
    state : state,
    getters : getters,
    mutations : mutations,
    actions : actions
};
