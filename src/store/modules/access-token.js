import $cookies from 'js-cookie';

/**
 * cookie 名称
 * @type {string}
 */
const COOKIE_NAME = 'at';

//  初始化 state
const state = $cookies.get(COOKIE_NAME) ? JSON.parse($cookies.get(COOKIE_NAME)) : {
    token : undefined,
    flag : undefined
};

const getters = {
    getToken :  state => state.token,
    getFlag :   state => state.flag
};

const mutations = {
    setAccessToken(state,data){
        if (!data){
            state.token = undefined;
            state.flag = undefined;
            $cookies.remove(COOKIE_NAME);
            return;
        }
        state.token = data.token;
        state.flag = data.flag;
        $cookies.set(COOKIE_NAME, JSON.stringify(state) , 0, '/', window.location.hostname);
    },
};

const actions = {

};

export default {
    state : state,
    getters : getters,
    mutations : mutations,
    actions : actions
};
