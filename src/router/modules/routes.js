export default [
    {
        path : '/',
        redirect : { name : 'index' }
    },{
        path :  '/index',
        name : 'index',
        meta :  { title : '首页' },
        component : () => import(/* webpackChunkName: "views" */ "../../views/Index"),
    },{
        path : '/console',
        name : 'console',
        meta :  { title : '控制台', permission : '' },
        component : () => import(/* webpackChunkName: "views" */ "../../views/console/Index"),
    },{
        path : '/register',
        name : 'register',
        meta :  { title : '注册', permission : '' },
        component : () => import(/* webpackChunkName: "views" */ "../../views/register/Index"),
        props : (route) => ({ callback : decodeURI(route.query.callback ? route.query.callback : '/index') })
    },{
        path : '/oauth',
        name : 'oauth',
        meta :  { title : '登录' },
        component : () => import(/* webpackChunkName: "views" */ "../../views/oauth/Index"),
        props : (route) => ({
            code : route.query.code,
            flag : route.query.flag,
            callback : decodeURI(route.query.callback ? route.query.callback : '/index')
        })
    },{
        path : '/logout',
        name : 'logout',
        meta :  { title : '退出' },
        component : () => import(/* webpackChunkName: "views" */ "../../views/oauth/Logout"),
    },{
        path : '/client/add',
        name : 'addClient',
        meta :  { title : '创建客户端', permission : '' },
        component : () => import(/* webpackChunkName: "views" */ "../../views/client/AddClient"),
    },{
        path : '/client/info/:id',
        name : 'clientInfo',
        meta :  { title : '客户端详情', permission : '' },
        component : () => import(/* webpackChunkName: "views" */ "../../views/client/ClientInfo"),
        props : (route) => ({ clientId: route.params.id })
    }
];
