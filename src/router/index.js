import Vue from "vue";
import VueRouter from "vue-router";
import store from '../store/index';

/* 引入 NProgress 组件 */
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';

Vue.use(VueRouter);


const router = new VueRouter({
    base: process.env.VUE_APP_BASE_URL
});


//  全局前置守卫
router.beforeEach((to,form,next) =>{
    NProgress.start();
    //  判断资源是否需要登录可以访问
    let permission = to.meta.permission;
    if (undefined !== permission){
        //  跳转到登录页面
        if (!store.getters.isLogin){
            window.location.href = store.getters.getOauthAuthorizeUrl + '&callback=' + encodeURI(to.fullPath);
            return;
        }
        //  权限校验
    }
    next();
});

//  全局后置钩子
router.afterEach(() => {
    NProgress.done();
});

//  添加路由规则
import routes from './modules/routes';

router.addRoutes([ ...routes ]);

export default router;
