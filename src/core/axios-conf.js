import axIos from 'axios';

import store from '../store';
import Vue from 'vue';
import $router from '../router/index';

// 请求拦截器
axIos.interceptors.request.use(
    // 在发送请求之前做点什么
    (config) => {
        //  携带访问令牌
        let accessToken = store.getters.getToken;
        if (accessToken && !config.headers['Authorization']){
            config.headers['Authorization'] = 'Bearer ' + accessToken;
            config.headers['flag'] = store.getters.getFlag;
        }
        return config;
    },
    //  处理请求错误
    (error) => Promise.reject(error)
);

/**
 * 自定义异常
 * @param code 状态码
 * @param msg 异常信息
 * @constructor
 */
const HttpError = function(code,msg){
    this.code = code;
    this.msg = msg;
};
HttpError.prototype = Object.create(Error.prototype);

// 添加响应拦截器
axIos.interceptors.response.use(
    //  处理响应数据
    (response) =>  {
        switch (response.data.code) {
            case 200:break;
            //  登录超时
            case 407:
                //  清空过期令牌和用户信息
                store.commit('setAccessToken',null);
                this.$store.commit('delUserInfo');
                //  弹窗提示,关闭后刷新页面,会自动跳转到登陆页面
                Vue.prototype.$modal.error({
                    title : response.data.code,
                    content : '登录超时,请重新登陆',
                    onOk : () =>
                        window.location.href =
                            store.getters.getOauthAuthorizeUrl +
                            '&callback=' + encodeURI($router.app.$route.fullPath)
                });
                throw new HttpError(response.data.code,response.data.msg);
            //  其他异常
            default:
                Vue.prototype.$modal.error({
                    title : response.data.code,
                    content : response.data.msg,
                });
                throw new HttpError(response.data.code,response.data.msg);
        }
        return response.data;
    },
    // 处理响应错误
    (error) => {
        if (error instanceof Error) {
            Vue.prototype.$modal.error({ content : error.message });
        }
        return Promise.reject(error);
    }
);
