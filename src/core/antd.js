import Vue from 'vue';

/* 引入 ant-design-vue 组件 */
import {
    Layout,Modal,Menu,ConfigProvider,Spin,Card,List,Button,Form,
    Row,Col,Pagination,Avatar,Empty,Statistic,Tabs,Input,Icon,Steps
} from 'ant-design-vue';

Vue.prototype.$modal = Modal;

import $message from 'ant-design-vue/lib/message/index';
import 'ant-design-vue/lib/message/style/index';
Vue.prototype.$message = $message;


Vue.use(Layout);
Vue.use(Modal);
Vue.use(Menu);
Vue.use(ConfigProvider);
Vue.use(Spin);
Vue.use(Card);
Vue.use(List);
Vue.use(Button);
Vue.use(Form);
Vue.use(Row);
Vue.use(Col);
Vue.use(Pagination);
Vue.use(Avatar);
Vue.use(Empty);
Vue.use(Statistic);
Vue.use(Tabs);
Vue.use(Input);
Vue.use(Icon);
Vue.use(Steps);
