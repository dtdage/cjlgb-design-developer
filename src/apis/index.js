import axIos from 'axios';

/**
 * 认证接口
 * @param code
 * @param flag
 * @returns {Promise<AxiosResponse<T>>}
 */
export function grant(code,flag) {
    return axIos.get("/apis/cjlgb-design-developer/developer/grant/" + code + "/" + flag);
}

/**
 * 获取当前开发者信息
 * @returns {Promise<AxiosResponse<T>>}
 */
export function getMyInfo() {
    return axIos.get("/apis/cjlgb-design-developer/developer/getMyInfo");
}

/**
 * 保存开发者信息
 * @param params
 * @returns {Promise<AxiosResponse<T>>}
 */
export function saveDeveloper(params) {
    return axIos.post("/apis/cjlgb-design-developer/developer",params)
}

/**
 * 分页查询客户端列表
 * @param params
 * @returns {Promise<AxiosResponse<T>>}
 */
export function pagingQueryClient(params) {
    return axIos.get("/apis/cjlgb-design-developer/client/page",{ params : params });
}

/**
 * 创建客户端
 * @param params
 */
export function addOauthClient(params) {
    return axIos.post("/apis/cjlgb-design-developer/client",params);
}

/**
 * 获取客户端信息
 * @param clientId
 * @returns {Promise<AxiosResponse<T>>}
 */
export function getOauthClient(clientId) {
    return axIos.get("/apis/cjlgb-design-developer/client/" + clientId);
}
